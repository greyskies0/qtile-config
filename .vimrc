" BASIC SETTINGS {{{
syntax on
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set relativenumber
set numberwidth=6
set nowrap
set smartcase
set incsearch
set scrolloff=8
set mouse=a
set cursorline
set termguicolors
set splitbelow splitright
set wildmenu
set wildmode=longest:full,full
set nobackup       "no backup files
set noswapfile     "no swap files
set hidden
set foldmethod=marker
"}}}

" PLUGINS {{{
call plug#begin('~/.vim/plugged')

Plug 'vim-syntastic/syntastic'
Plug 'preservim/tagbar'
Plug 'szw/vim-maximizer'
Plug 'junegunn/goyo.vim'
Plug 'ghifarit53/tokyonight-vim'
Plug 'Chiel92/vim-autoformat'
Plug 'rafamadriz/friendly-snippets'
Plug 'jiangmiao/auto-pairs'
Plug 'elkowar/yuck.vim'

call plug#end()
" }}}

" PLUGIN SETTINGS AND KEYBINDINGS {{{
let mapleader = " "

" GOYO {{{
nnoremap <leader>go :Goyo<CR>
" }}}

" PLUGGED CLEANING AND UPGRADING {{{
nnoremap <leader>pc :PlugClean<CR>
nnoremap <leader>pu :PlugUpdate<CR>
" }}}

" COLORSCHEME {{{
let g:tokyonight_style = 'night'
let g:tokyonight_enable_italic = 1

colorscheme tokyonight
" }}}

" TOGGLING WRAPPING OF LINES {{{
nmap <C-w> :set wrap!<CR>
" }}}

" SYNTASTIC SETTINGS {{{
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" }}}

" TAGBAR SETTINGS {{{
nmap <C-e> :TagbarToggle<CR>
" }}}

" SPLIT SETTINGS {{{
nmap <C-s> :vs<CR>
nmap <C-x> :sp<CR>
" moving between splits
nnoremap <C-l> <C-w>l
nnoremap <C-h> <C-w>h
nnoremap <C-k> <C-w>k
nnoremap <C-j> <C-w>j
" resizing splits
nmap <C-Right> :vertical resize +2<CR>
nmap <C-Left> :vertical resize -2<CR>
nmap <C-Up> :resize +2<CR>
nmap <C-Down> :resize -2<CR>
" }}}

" MAXIMIZE WINDOW {{{
noremap <C-m> :MaximizerToggle!<CR>
" }}}

" UNDO TWEAK {{{
inoremap , ,<c-g>u
inoremap . .<c-g>u
" }}}

" MOVING LINES {{{
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
inoremap <C-j> <esc>:m .+1<CR>==i
inoremap <C-k> <esc>:m .-2<CR>==i
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==
" }}}

" AUTOFORMAT {{{
nnoremap <leader>form :Autoformat<CR>
" }}}

" COQ {{{
let g:coq_settings = { 'auto_start': 'shut-up' }
" }}}
" }}}
