# My Qtile dotfiles

## Install script
```bash
# fast way:
$ curl -fsSl https://gitlab.com/greyskies0/qtile-config/-/raw/master/deploy.sh | sh

# safe way:
$ mkdir ~/git && cd ~/git
$ git clone https://gitlab.com/greyskies0/qtile-config.git && cd qtile-config
$ ./deploy.sh   # remember to delete the `git clone ...` part!
```
