#!/bin/bash

set -xe

yay -Sy paru-bin --noconfirm --needed

mkdir "$HOME"/git; cd "$HOME"/git
git clone https://gitlab.com/greyskies0/qtile-config.git

cd "$HOME"/git/qtile-config && \
    paru -Syy --needed - < packages
paru -Syyu --noconfirm

# PRZENOSZENIE PLIKÓW
cp          "$HOME"/git/qtile-config/tapety/tapeta.png "$HOME"/Obrazy/
mkdir -p    "$HOME"/.config/qtile
mkdir -p    "$HOME"/.config/alacritty
mkdir -p    "$HOME"/.config/nvim
cp -r       "$HOME"/git/qtile-config/qtile/scripts/* \
            "$HOME"/.config/qtile/scripts/
ln -s       "$HOME"/git/qtile-config/qtile/config.py \
            "$HOME"/.config/qtile/config.py
ln -s       "$HOME"/git/qtile-config/alacritty.toml "$HOME"/.config/alacritty/
ln -s       "$HOME"/git/qtile-config/.vimrc "$HOME"/.vimrc
ln -rs      "$HOME"/git/qtile-config/nvim "$HOME"/.config/nvim

# VIM PLUG
curl -fLo "$HOME"/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
# neovim
nvim -es -i NONE -c "PackerInstall" -c "qa"
nvim -es -i NONE -c "PackerSync" -c "qa"
nvim -es -i NONE -c "PackerCompile" -c "qa"
nvim -es -i NONE -c "COQdeps" -c "qa"
nvim -es -i NONE -c "TSInstall cpp python bash" -c "qa"

# MOTYW
cd "$HOME" && mkdir -p .themes
cd "$HOME"/.themes
git clone https://github.com/material-ocean/Gtk-Theme.git Material-Ocean && cd "$HOME"
mkdir -p "$HOME"/.config/rofi
ln "$HOME"/git/qtile-config/rofi/config.rasi "$HOME"/.config/rofi/config.rasi
ln "$HOME"/git/qtile-config/rofi/material-ocean.rasi "$HOME"/.config/rofi/material-ocean.rasi
pip install pywalfox && pywalfox install && chmod +x /home/maciek/.local/lib/python3.9/site-packages/pywalfox/bin/main.sh

# PODSWIETLENIE DO MYSZKI
notify-send 'skrypt:' 'instalacja programu od podswietlenia do myszki'
sudo pip3 install rivalcfg
sudo rivalcfg --update-udev

# MISC
python3 -m pip install --user virtualenv
python3 -m pip install pynvim

# EMACS
notify-send 'skrypt:' 'instalacja doom emacs'
cd "$HOME" && git clone --depth 1 https://github.com/hlissner/doom-emacs "$HOME"/.emacs.d && "$HOME"/.emacs.d/bin/doom install
"$HOME"/.emacs.d/bin/doom sync && "$HOME"/.emacs.d/bin/doom doctor
cd "$HOME" && rm -rf "$HOME"/.doom.d; git clone https://gitlab.com/greyskies0/emacs-config.git .doom.d
"$HOME"/.emacs.d/bin/doom sync && "$HOME"/.emacs.d/bin/doom doctor

# OH MY ZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
rm -rf "$HOME"/.zshrc && ln "$HOME"/git/qtile-config/.zshrc "$HOME"/.zshrc

# STEAM
paru -S steam

# SPICETIFY
paru -S spotify && \
    curl -fsSL https://raw.githubusercontent.com/spicetify/spicetify-cli/master/install.sh | sh && \
    curl -fsSL https://raw.githubusercontent.com/spicetify/spicetify-marketplace/main/install.sh | sh

topgrade -y

mv "$HOME"/git/qtile-config/fonts/AR\ PL\ New\ Kai/* "$HOME"/.local/share/fonts/

notify-send 'skrypt:' '<b>GOTOWE</b>'
