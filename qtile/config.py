# IMPORTS {{{
import os
import subprocess
from os.path import exists
from libqtile import qtile
from libqtile.config import Key, Screen, Group, Drag, Match
from libqtile.command import lazy
from libqtile import layout, bar, hook
from qtile_extras import widget # type: ignore
from qtile_extras.widget.decorations import RectDecoration # type: ignore

# }}}

# KEYBINDINGS {{{
# mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser("~")


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

keys = [
    # SUPER + FUNCTION KEYS
    Key([mod], "f",                 lazy.window.toggle_fullscreen()),
    Key([mod, "shift"], "c",        lazy.window.kill()),
    Key([mod], "Return",            lazy.spawn("alacritty")),
    Key([mod, "control"], "Return", lazy.spawn("alacritty --class=alacritty-floating")),
    Key([mod], "b",                 lazy.spawn("firefox")),
    Key([mod], "r",                 lazy.spawn("rofi -show drun")),
    Key([mod], "e",                 lazy.spawn("emacs")),
    # SUPER + SHIFT KEYS
    Key([mod, "shift"], "Return", lazy.spawn("pcmanfm")),
    Key([mod, "shift"], "q",      lazy.window.kill()),
    Key([mod, "shift"], "r",      lazy.restart()),
    # SCREENSHOTS
    Key([], "Print",             lazy.spawn("scrot 'screen-%s.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'")),
    Key([mod], "Print",          lazy.spawn("gnome-screenshot -i")),
    Key([mod, "shift"], "Print", lazy.spawn("xfce-screenshooter")),
    # INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp",   lazy.spawn("brightnessctl set +10%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-")),
    # INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute",        lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl --player spotify,%any play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl --player spotify,%any next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl --player spotify,%any previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl --player spotify,%any stop")),
    # QTILE LAYOUT KEYS
    Key([mod], "n",     lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),
    # CHANGE FOCUS
    Key([mod], "Up",    lazy.layout.up()),
    Key([mod], "Down",  lazy.layout.down()),
    Key([mod], "Left",  lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    # RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
    # MOVE WINDOWS BSP LAYOUT
    Key([mod, "shift"], "Down",  lazy.layout.flip_down()),
    Key([mod, "shift"], "Up",    lazy.layout.flip_up()),
    Key([mod, "shift"], "Left",  lazy.layout.flip_left()),
    Key([mod, "shift"], "Right", lazy.layout.flip_right()),
    Key([mod, "shift"], "j",     lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k",     lazy.layout.shuffle_up()),
    Key([mod, "shift"], "h",     lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l",     lazy.layout.shuffle_right()),
    # TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),
    # F1-F12 STUFF
    # Key([], "F12", lazy.spawn("/home/maciek/.config/eww/toggle")) # doesn't work :(
]
# }}}

# GROUPS AND THEME {{{
group_names = [
    "一", # 1
    "二", # 2
    "三", # 3
    "四", # 4
    "五", # 5
    "六", # 6
    "七", # 7
    "八", # 8
    "九", # 9
]

groups = [
    Group(name = "1", label = group_names[0], layout = "max", matches=[
        Match(wm_class="firefox"),
    ]),
    Group(name = "2", label = group_names[1], layout = "max", matches=[
        Match(wm_class="Emacs")
    ]),
    Group(name = "3", label = group_names[2], layout = "max"),
    Group(name = "4", label = group_names[3], layout = "max", matches=[
        Match(wm_class="Steam"),
        Match(title="Steam")
    ]),
    Group(name = "5", label = group_names[4], layout = "max", matches=[
        Match(wm_class="discord"),
        Match(title="Discord Updater")
    ]),
    Group(name = "6", label = group_names[5], layout = "max", matches=[
        Match(wm_class="Spotify"),
        Match(title="Spotify"),
    ]),
]

for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen(toggle=True),
            desc="Switch to group {}".format(i.name)),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True, toggle=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
    ])

def init_layout_theme():
    return {
        "margin": 3,
        "border_width": 2,
        "border_focus": "#E63052",
        "border_normal": "#535061",
    }
def init_max_layout_theme():
    return {
        "margin": 0,
        "border_width": 0,
    }

layout_theme = init_layout_theme()
max_layout_theme = init_max_layout_theme()

layouts = [
    layout.Max(**max_layout_theme),
    layout.Bsp(**layout_theme),
]

def init_colors():
    return [
        ["#0f111a", "#0f111a"],  # color 0 - black
        ["#E63052", "#E63052"],  # color 1 - red
        ["#FDFDFD", "#FDFDFD"],  # color 2 - white
        ["#131621", "#131621"],  # color 3 - grey
        ["#181c29", "#181c29"],  # color 4 - light grey
    ]

colors = init_colors()

def init_widgets_defaults():
    return dict(
        font = "FiraCode Nerd Font Mono", fontsize = 12, padding = 2, background = colors[0]
    )

decor = {
    "decorations": [
        RectDecoration(colour = colors[4], radius = 10, filled = True, padding_y = 3)
    ],
    "padding": 10,
}

widget_defaults = init_widgets_defaults()
# }}}

# WIDGETS AND THE BAR {{{
def init_widgets_list_top():
    widgets_list = [
        widget.Sep(linewidth = 0, padding = 20, foreground = colors[3], background = colors[0]),
        widget.GroupBox(
            font = "AR PL New Kai",
            fontsize = 18,
            margin_y = 6,
            margin_x = 4,
            padding_y = 8,
            padding_x = 4,
            borderwidth = 3,
            disable_drag = True,
            active = colors[1],
            inactive = colors[2],
            highlight_color = colors[4],
            highlight_method = "line",
            this_current_screen_border = colors[1],
            this_screen_border = colors[1],
            foreground = colors[3],
            background = colors[0],
            hide_unused = True,
            **decor,
        ),
        widget.Spacer(),
        widget.Clock(
            foreground = colors[2], background = colors[0], format = "%H:%M:%S",
            mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("/home/maciek/git/eww/target/release/eww open settings"),
                               'Button3': lambda: qtile.cmd_spawn("/home/maciek/git/eww/target/release/eww close settings")},
            **decor,
        ),
        widget.Spacer(),
        widget.TextBox(
            fontsize = 18,
            text = ""
        ),
        widget.MemoryGraph(
            graph_color = colors[1],
            fill_color = colors[1],
            border_color = colors[4],
            margin_y = 6
        ),
        widget.TextBox(
            fontsize = 22,
            text = ""
        ),
        widget.CPUGraph(
            graph_color = colors[1],
            fill_color = colors[1],
            border_color = colors[4],
            margin_y = 6
        ),
        widget.Volume(
            theme_path = "/usr/share/icons/Tela-red-dark",
            #theme_path = "/usr/share/icons/Tela-red-dark/24/panel/",
            #mode = 'icon',
            #bar_colour_loud = colors[0],
            #bar_colour_high = colors[0],
            #bar_colour_normal = colors[0],
            #bar_colour_mute = colors[1],
            #bar_width = 40,
            #hide_interval = 1,
            padding = 0,
            mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn("pavucontrol")},
            update_interval = 1,
        ),
        widget.Systray(
            background = colors[0],
            icon_size = 22,
        ),
        widget.Sep(linewidth = 0, padding = 20, foreground = colors[3], background = colors[0]),
    ]
    if exists("/sys/class/power_supply/BAT0"):
        widgets_list.insert(9, widget.UPowerWidget(
            percentage_low = 0.35,
            percentage_critical = 0.15,
            text_charging = '[{percentage:.0f}% | {ttf}]',
            text_discharging = '[{percentage:.0f}% | {tte}]',
            border_charge_colour = '33b8ff',
            border_critical_colour = colors[1],
            fill_low = '3afc47',
            fill_critical = colors[1],
        ))
    return widgets_list

def init_widgets_list_bot():
    widgets_list = [
        widget.Sep(linewidth = 0, padding = 10, foreground = colors[3], background = colors[0]),
        widget.TaskList(
            foreground = colors[2],
            background = colors[0],
            border = colors[4],
            unfocused_border = colors[0],
            max_title_width = None,
            margin_y = 3,
            margin_x = 3,
            padding_y = 4,
            padding_x = 8,
            highlight_method = "block",
            icon_size = 17,
            spacing = 3,
            txt_floating = " ",
            txt_minimized = " ",
            title_width_method = "uniform",
        ),
        widget.Sep(linewidth = 0, padding = 10, foreground = colors[3], background = colors[0]),
    ]
    return widgets_list

widgets_top = init_widgets_list_top()
widgets_bot = init_widgets_list_bot()

def init_screens():
    return [
        Screen(
            top = bar.Bar(
                widgets = widgets_top,
                size = 30,
                margin = [3, 10, 3, 10],
            ),
            bottom = bar.Bar(
                widgets = widgets_bot,
                size = 30,
                margin = [3, 10, 3, 10],
            ),
            left = bar.Gap(20),
            right = bar.Gap(20),
        )
    ]

screens = init_screens()
# }}}

# MISC {{{
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start = lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),     start = lazy.window.get_size()),
]

dgroups_key_binder = None

main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/scripts/autostart.sh"])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(["xsetroot", "-cursor_name", "left_ptr"])

@hook.subscribe.client_new
def set_floating(window):
    if ( window.window.get_wm_transient_for() or window.window.get_wm_type() in floating_types ):
        window.floating = True
        window.cmd_center()

# }}}

# WINDOW SETTINGS {{{
floating_types = ["notification", "toolbar", "splash", "dialog"]

follow_mouse_focus = True
bring_front_click = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules = [
        Match(wm_class = "confirm"),
        Match(wm_class = "dialog"),
        Match(wm_class = "download"),
        Match(wm_class = "error"),
        Match(wm_class = "file_progress"),
        Match(wm_class = "notification"),
        Match(wm_class = "splash"),
        Match(wm_class = "toolbar"),
        Match(wm_class = "confirmreset"),
        Match(wm_class = "makebranch"),
        Match(wm_class = "maketag"),
        Match(wm_class = "Arandr"),
        Match(wm_class = "Galculator"),
        Match(wm_class = "ssh-askpass"),
        Match(wm_class = "zoom"),
        Match(wm_class = "pavucontrol"),
        Match(wm_class = "gimp-2.10"),
        Match(wm_class = "qalculate-gtk"),
        Match(wm_class = "lxpolkit"),
        Match(wm_class = "alacritty-floating"),
        Match(wm_class = "VirtualBox Manager"),
        Match(wm_class = "Gnome-screenshot"),
        Match(title = "branchdialog"),
        Match(title = "Open File"),
        Match(title = "pinentry"),
        Match(title = "Biblioteka"),
    ],
    fullscreen_border_width = 0,
    border_width = 2,
    border_focus = "#242a4a",
    border_normal = "#171b2e"
)
auto_fullscreen = True
auto_minimize = True
focus_on_window_activation = "smart"  # or focus
# }}}

wmname = "qtile"
