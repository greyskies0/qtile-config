#!/bin/bash

#function run {
#  if ! pgrep $1 ;
#  then
#    "$@"&
#  fi
#}


#starting utility applications at boot time
#volumeicon &
lxsession &
nm-applet &
xfce4-power-manager &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
picom --experimental-backend --detect-rounded-corners -b --config "$HOME"/.config/qtile/scripts/picom.conf &

#starting user applications at boot time
nitrogen --restore &
fusuma &
thunderbird --headless &
