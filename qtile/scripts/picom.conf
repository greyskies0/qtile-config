blur:
{
    method = "dual_kawase";
    strength = 10;
    kern = "3x3box";
}

rounded-corners-exclude = [
  "class_g = 'Rofi'",
  "class_g = 'Xfce4-notifyd'",
  "class_g = 'dmenu'",
  "class_g = 'volumeicon'"
];
corner-radius = 4;
round-borders = 1;

# Backend to use: "xrender" or "glx".
# GLX backend is typically much faster but depends on a sane driver.
backend = "glx";
glx-no-stencil = true;
glx-copy-from-front = false;

# GLX backend: Avoid rebinding pixmap on window damage.
# Probably could improve performance on rapid window content changes, but is known to break things on some drivers (LLVMpipe).
# Recommended if it works.
glx-no-rebind-pixmap = true;

# Enabled client-side shadows on windows.
shadow = true;
# The blur radius for shadows. (default 12)
shadow-radius = 20;
# The left offset for shadows. (default -15)
shadow-offset-x = -20;
# The top offset for shadows. (default -15)
shadow-offset-y = -20;
# The translucency for shadows. (default .75)
shadow-opacity = 0.4;

log-level = "warn";
#change your username here
#log-file = "/home/maciek/.config/compton.log";

# shadow-red = 0.0;
# shadow-green = 0.0;
# shadow-blue = 0.0;

shadow-exclude = [
    "! name~=''",
    "name = 'Notification'",
    "name = 'Plank'",
    "name = 'Docky'",
    "name = 'Kupfer'",
#    "name = 'xfce4-notifyd'",
    "name *= 'VLC'",
    "name *= 'compton'",
    "name *= 'Chromium'",
    "name *= 'Chrome'",
#    "class_g = 'Firefox' && argb",
    "class_g = 'Conky'",
    "class_g = 'Kupfer'",
    "class_g = 'Synapse'",
    "class_g = 'Xfce4-screenshooter'",
    "class_g = 'zoom'",
    "class_g ?= 'Notify-osd'",
    "class_g ?= 'Cairo-dock'",
    "class_g = 'Cairo-clock'",
#    "class_g ?= 'Xfce4-notifyd'",
    "class_g ?= 'Xfce4-power-manager'",
    "_GTK_FRAME_EXTENTS@:c",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'"
];
shadow-ignore-shaped = false;

inactive-opacity = 1;
active-opacity = 1;
frame-opacity = 1;
inactive-opacity-override = true;

opacity-rule = [
	"100:class_g = 'Alacritty'",
	"90:class_g  = 'Spotify'",
    "90:class_g  = 'Pcmanfm'",
	"80:class_g  = 'Rofi'",
	"80:class_g  = 'Xfce4-notifyd'"
];

# Dim inactive windows. (0.0 - 1.0)
# inactive-dim = 0.2;
# Do not let dimness adjust based on window opacity.
# inactive-dim-fixed = true;
blur-background = true;
blur-background-fixed = false;
blur-background-exclude = [
    "window_type = 'dock'",
    "window_type = 'desktop'",
    "_GTK_FRAME_EXTENTS@:c",
    "class_g = 'Xfce4-screenshooter'",
    "class_g = 'Gnome-screenshot'",
    "class_g = 'zoom'"
];

# Fade windows during opacity changes.
fading = true;
# The time between steps in a fade in milliseconds. (default 10).
fade-delta = 4;
# Opacity change between steps while fading in. (default 0.028).
fade-in-step = 0.03;
# Opacity change between steps while fading out. (default 0.03).
fade-out-step = 0.03;
no-fading-openclose = false;

fade-exclude = [ ];
mark-wmwin-focused = true;
mark-ovredir-focused = true;
use-ewmh-active-win = true;
# Detect rounded corners and treat them as rectangular when --shadow-ignore-shaped is on.
detect-rounded-corners = true;
detect-client-opacity = true;

# Specify refresh rate of the screen.
# If not specified or 0, compton will try detecting this with X RandR extension.
refresh-rate = 0;
vsync = true;
dbe = false;

# Limit compton to repaint at most once every 1 / refresh_rate second to boost performance.
# This should not be used with --vsync drm/opengl/opengl-oml as they essentially does --sw-opti's job already,
# unless you wish to specify a lower refresh rate than the actual value.
#sw-opti = true;

unredir-if-possible = false;
focus-exclude = [ ];
detect-transient = true;
detect-client-leader = true;

wintypes:
{
  tooltip = { fade = true; shadow = true; opacity = 0.9; focus = true;};
  dock = { shadow = false; }
  dnd = { shadow = false; }
  popup_menu = { opacity = 0.9; }
  dropdown_menu = { opacity = 0.9; }
};

xrender-sync-fence = true;
