local let = vim.g
local home = os.getenv('HOME')


-- COLORIZER
require('colorizer').setup()


-- NVIM TREE
require('nvim-tree').setup()


-- TREESITTER
require('nvim-treesitter.configs').setup {
  ensure_installed = { "c", "cpp", "lua", "rust", "python", "bash", "latex" },
  sync_install = false,
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
}
require('treesitter-context').setup {
  enable = true,
  max_lines = 0,
  trim_scope = 'outer',
  patterns = {
    default = {
      'class',
      'function',
      'method',
      'for',
      'while',
      'if',
      'switch',
      'case',
    },
    tex = {
      'chapter',
      'section',
      'subsection',
      'subsubsection',
    },
    rust = {
      'impl_item',
      'struct',
      'enum',
    },
    markdown = {
      'section',
    },
  },
  exact_patterns = {
    -- Example for a specific filetype with Lua patterns
    -- Treat patterns.rust as a Lua pattern (i.e "^impl_item$" will
    -- exactly match "impl_item" only)
    -- rust = true,
  },

  -- [!] The options below are exposed but shouldn't require your attention,
  --     you can safely ignore them.

  zindex = 20,
  mode = 'cursor',
  separator = nil,
}


-- LSP and COQ
local lsp = require('lspconfig')
vim.diagnostic.config({
  virtual_text = true,
})
local lsp_flags = {
  debounce_text_changes = 150,
}
local on_attach = function(client, bufnr)
end
require('lspconfig').clangd.setup {
  on_attach = on_attach,
  flags = lsp_flags
}
require('lspconfig')['pyright'].setup {
  on_attach = on_attach,
  flags = lsp_flags,
}
require('lspconfig')['pylsp'].setup {
  on_attach = on_attach,
  flags = lsp_flags,
}
require('lspconfig')['anakin_language_server'].setup {
  on_attach = on_attach,
  flags = lsp_flags,
}
require('lspconfig')['rust_analyzer'].setup {
  on_attach = on_attach,
  flags = lsp_flags,
  settings = {
    ["rust-analyzer"] = {}
  }
}

local coq = require('coq')
let.coq_settings = { auto_start = 'shut-up' }
lsp.clangd.setup(coq.lsp_ensure_capabilities())
let.completion_matching_strategy_list = { 'exact', 'substring', 'fuzzy' }


-- BUFFERLINE
require('bufferline').setup()


-- TOGGLETERM
require('toggleterm').setup()


-- LUALINE
require('lualine').setup()
let.lightline = { colorscheme = 'tokyonight' }


-- SYMBOLS-OUTLINE
require('symbols-outline').setup()


-- MASON
require('mason').setup()


-- IMPATIENT
require('impatient')
