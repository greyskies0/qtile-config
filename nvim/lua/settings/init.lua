local set = vim.opt

set.cursorline      = true
set.expandtab       = true
set.hidden          = true
set.ignorecase      = true
set.incsearch 	    = true
set.number 	        = true
set.rnu  	        = true
set.smartcase       = true
set.smartindent     = true
set.splitbelow      = true
set.splitright      = true
set.termguicolors   = true
set.wildmenu        = true

set.numberwidth     = 6
set.scrolloff       = 8
set.shiftwidth	    = 4
set.softtabstop     = 4
set.tabstop	        = 4 

set.clipboard       = "unnamedplus"
set.colorcolumn     = "80"
set.foldmethod      = "marker"
set.mouse           = "a"
set.wildmode        = "longest:full,full"
