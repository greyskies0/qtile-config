local let = vim.g

let.tokyonight_italic_functions = 1
let.tokyonight_sidebars = { "qf", "vista_kind", "terminal", "packer" }
vim.cmd[[colorscheme tokyonight-night]]
