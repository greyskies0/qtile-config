local let = vim.g
local map = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

let.mapleader = " "

-- toggle wrap
map('n', '<C-w>', ':set wrap!<CR>', opts)

-- side bars toggles
map('n', '<C-q>', ':NvimTreeToggle<CR>', opts)
map('n', '<C-e>', ':SymbolsOutline<CR>', opts)

-- split things
map('n', '<C-s>', ':vs<CR>', opts)
map('n', '<C-x>', ':sp<CR>', opts)
map('n', '<C-l>', '<C-w>l',  opts)
map('n', '<C-h>', '<C-w>h',  opts)
map('n', '<C-k>', '<C-w>k',  opts)
map('n', '<C-j>', '<C-w>j',  opts)
map('n', '<C-Right>', ':vertical resize +2<CR>', opts)
map('n', '<C-Left>',  ':vertical resize -2<CR>', opts)
map('n', '<C-Up>',    ':resize +2<CR>',      opts)
map('n', '<C-Down>',  ':resize -2<CR>',      opts)

-- undo tweaks
map('i', ',', ',<c-g>u', opts)
map('i', '.', '.<c-g>u', opts)

-- moving multiple lines
map('i', '<C-j>',     '<esc>:m .+1<CR>==i', opts)
map('i', '<C-k>',     '<esc>:m .-2<CR>==i', opts)
map('n', '<leader>j', ':m .+1<CR>==',       opts)
map('n', '<leader>k', ':m .-2<CR>==',       opts)

-- popup terminal
map('n', '<leader>tf', ':ToggleTerm direction=float<CR>',              opts)
map('n', '<leader>th', ':ToggleTerm size=20 direction=horizontal<CR>', opts)
map('n', '<leader>tv', ':ToggleTerm size=80 direction=vertical<CR>',   opts)

-- autoformat
map('n', '<leader>form', ':Autoformat<CR>', opts)

-- lsp stuff
map('n', '<leader>gd', ':LspDefinition<CR>',     opts)
map('n', '<leader>pd', ':LspPeekDefinition<CR>', opts)
map('n', '<leader>gb', '<C-o>',                  opts)

-- telescope / buffers stuff
map('n', '<leader>grep', ':Telescope live_grep prompt_prefix=🔍 <cr>',  opts)
map('n', '<leader>help', ':Telescope live_grep cwd=~/.config/nvim/lua/keys prompt_prefix=🔍 <cr>',  opts)
map('n', '<leader>ff',   ':Telescope find_files prompt_prefix=🔍 <cr>', opts)
map('n', '<leader>fb',   ':Telescope buffers prompt_prefix=🔍 <cr>',    opts)
map('n', '<leader>hh',   ':BufferLineCycleNext<CR>', opts)
map('n', '<leader>ll',   ':BufferLineCyclePrev<CR>', opts)
map('n', '<leader>qq',   ':bd<CR>', opts)

-- clear search
map('n', '<leader>/', ':let @/ = \"\"<CR>', opts)

-- toggle diagnostics
map('n', '<leader>er', ":TroubleToggle<CR>", opts)
