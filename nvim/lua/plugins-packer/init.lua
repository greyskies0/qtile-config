return require('packer').startup(function()
  use 'kyazdani42/nvim-tree.lua'
  use 'norcalli/nvim-colorizer.lua'
  use 'preservim/tagbar'
  use 'lewis6991/impatient.nvim'
  use {
    'goolord/alpha-nvim',
    requires = { 'kyazdani42/nvim-web-devicons' },
    config = function ()
        require'alpha'.setup(require'alpha.themes.startify'.config)
    end
  }
  use 'neovim/nvim-lsp'
  use 'neovim/nvim-lspconfig'
  use {
    'ms-jpq/coq_nvim',
    branch = 'coq',
    run = ':COQdeps'
  }
  use {
    'ms-jpq/coq.artifacts',
    branch = 'artifacts'
  }
  use {
    'ms-jpq/coq.thirdparty',
    branch = '3p'
  }
  use 'Chiel92/vim-autoformat'
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }
  use 'nvim-treesitter/nvim-treesitter-context'
  use 'nvim-treesitter/playground'
  use 'nvim-lua/plenary.nvim'
  use 'nvim-telescope/telescope.nvim'
  use 'kyazdani42/nvim-web-devicons'
  use {
    'akinsho/bufferline.nvim',
    tag = 'v2.*'
  }
  use 'lukas-reineke/indent-blankline.nvim'
  use 'rafamadriz/friendly-snippets'
  use 'akinsho/toggleterm.nvim'
  use 'jiangmiao/auto-pairs'
  use 'elkowar/yuck.vim'
  use 'folke/tokyonight.nvim'
  use 'nvim-lualine/lualine.nvim'
  use 'cdelledonne/vim-cmake'
  use 'fladson/vim-kitty'
  use 'folke/lsp-colors.nvim'
  use {
    "folke/trouble.nvim",
    requires = "kyazdani42/nvim-web-devicons",
    config = function()
      require("trouble").setup {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
        auto_open = true, -- automatically open the list when you have diagnostics
        auto_close = true, -- automatically close the list when you have no diagnostics
      }
    end
  }
  use 'simrat39/symbols-outline.nvim'
  use 'NoahTheDuke/vim-just'
  use 'williamboman/mason.nvim'
end)
