-- Commands to run after nvim launches

vim.cmd("PackerSync")
vim.cmd("LspStart")
vim.cmd("COQnow")
