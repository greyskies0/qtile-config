# EXPORTS {{{
export PATH="$PATH:/home/maciek/scripts:/home/maciek/.local/bin:/home/maciek/.config/emacs/bin:/home/maciek/git/eww/target/release:$HOME/.spicetify:$HOME/go/bin:$HOME/.local/share/cargo/bin"
export EDITOR="helix"
export MANROFFOPT="-c"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

export HISTFILE="$XDG_STATE_HOME/bash/history"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export GOPATH="$XDG_DATA_HOME/go"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export ICEAUTHORITY="$XDG_CACHE_HOME/ICEauthority"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export HISTFILE="$XDG_STATE_HOME/zsh/history"

export ZSH=$HOME/.oh-my-zsh
# }}}

# HIGHLIGHTING {{{
typeset -A ZSH_HIGHLIGHT_STYLES
export ZSH_HIGHLIGHT_STYLES[suffix-alias]=fg=magenta,underline
export ZSH_HIGHLIGHT_STYLES[precommand]=fg=cyan,underline
export ZSH_HIGHLIGHT_STYLES[arg0]=fg=cyan
# }}}

# PROMPT {{{
# export ZSH_THEME="headline"
# }}}

# PLUGINS {{{
export plugins=(
  git
  zsh-autosuggestions
  zsh-syntax-highlighting
)
# }}}

# ZSH STUFF {{{
source $ZSH/oh-my-zsh.sh
# }}}

# FUNCTIONS {{{
function ex()    # ex <file> :: extracts an archive
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf "$1"    ;;
      *.tar.gz)    tar xzf "$1"    ;;
      *.bz2)       bunzip2 "$1"    ;;
      *.rar)       unrar x "$1"    ;;
      *.gz)        gunzip "$1"     ;;
      *.tar)       tar xf "$1"     ;;
      *.tbz2)      tar xjf "$1"    ;;
      *.tgz)       tar xzf "$1"    ;;
      *.zip)       unzip "$1"      ;;
      *.Z)         uncompress "$1" ;;
      *.7z)        7z x "$1"       ;;
      *.deb)       ar x "$1"       ;;
      *.tar.xz)    tar xf "$1"     ;;
      *.tar.zst)   unzstd "$1"     ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

function y() {
  local tmp="$(mktemp -t "yazi-cwd.XXXXXX")" cwd
  yazi "$@" --cwd-file="$tmp"
  if cwd="$(command cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
    builtin cd -- "$cwd"
  fi
  \rm -f -- "$tmp"
}
# }}}

# ALIASES {{{
if which tmux >/dev/null; then
  alias t='tmux'
fi
if which coreutils >/dev/null; then
  alias cp='coreutils cp'
  alias echo='coreutils echo'
  alias ln='coreutils ln'
  alias mkdir='coreutils mkdir'
  alias mv='coreutils mv'
  alias pwd='coreutils pwd'
  alias rm='coreutils rm'
  alias rmdir='coreutils rmdir'
  alias sleep='coreutils sleep'
  alias touch='coreutils touch'
fi
if which lsd >/dev/null; then
  alias lsd='lsd --group-dirs first'
  alias ls='lsd --group-dirs first' 
  alias la='lsd -A --group-dirs first' 
  alias ll='lsd -lAh --group-dirs first' 
  alias l='lsd -1Ah --group-dirs first' 
fi
if which eza >/dev/null; then
  alias l='eza -ahl --icons --no-user --no-time --git-repos --git'
  alias ll='eza -aBhl --icons --no-user --git-repos --git'
  alias lt='eza --long --tree --level=3 --icons --no-user --no-time --no-permissions --git-repos --git'
fi
alias htop='htop -t'
alias cleanup='paru -Rns $(paru -Qtdq)'
alias v='vim'
if which nvim >/dev/null; then
  alias nv='nvim'
  alias vim='nvim'
fi
if which helix >/dev/null; then
  alias hx='helix'
  alias hxf='helix $(find . -readable -type f -not -path "*/.git/*" | fzf)'
fi
if which fzf >/dev/null; then
  alias fzf='fzf --preview "bat --color=always --style=numbers --line-range=:500 {}"'
fi
alias mv='mv -v'
alias cp='cp -v'
alias rm='rm -v'
if which emacs >/dev/null; then
  alias ec='emacsclient --create-frame --frame-parameters="((fullscreen . maximized))"'
fi
if which bat >/dev/null; then
  alias cat='bat -pP --theme=base16'
  alias bat='bat --theme=base16'
fi
# }}}

# BINDS {{{
bindkey '^H' backward-kill-word
# }}}

eval "$(starship init zsh)"
